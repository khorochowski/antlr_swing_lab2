tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  Integer numer = 0;
}
prog    : (e+=expr | d+=decl)* -> template(name={$e},deklaracje={$d}) "<deklaracje; separator=\" \n\"> 
start: <name;separator=\" \n\"> ";

decl  :
        ^(VAR i1=ID) {globals.newSymbol($ID.text);} -> dek(n={$ID.text})
    ;
    catch [RuntimeException ex] {errorID(ex,$i1);}

expr    : ^(PLUS  e1=expr e2=expr) -> dodaj(p1={$e1.st},p2={$e2.st})
        | ^(MINUS e1=expr e2=expr) -> odejmij(p1={$e1.st},p2={$e2.st})
        | ^(MUL   e1=expr e2=expr) -> mnoz(p1={$e1.st},p2={$e2.st})
        | ^(DIV   e1=expr e2=expr) -> dziel(p1={$e1.st},p2={$e2.st})
        | ^(PODST i1=ID   e2=expr) -> przypisz(key={$ID.text},value={$e2.st})
        | INT  {numer++;}                    -> int(i={$INT.text},j={numer.toString()})
        | ^(CMP   e1=expr e2=expr) -> rowne(p1={$e1.st},p2={$e2.st})
        | ^(NCMP  e1=expr e2=expr) -> rozne(p1={$e1.st},p2={$e2.st})
        | ^(DO     e1=expr e2=expr) {numer++;}  -> do(exp1={$e1.st},exp2={$e2.st},n={numer.toString()})
        | ^(WHILE  e1=expr e2=expr) {numer++;}  -> while(exp1={$e1.st},exp2={$e2.st}, n={numer.toString()})
        | ^(FOR    e1=expr e2=expr) {numer++;}  -> for(exp1={$e1.st},exp2={$e2.st},n={numer.toString()})
        | ^(IF     e1=expr e2=expr e3=expr?) {numer++;} -> if(cond={$e1.st}, cond_true={$e2.st}, cond_false={$e3.st}, numer={numer.toString()})
    ;
    