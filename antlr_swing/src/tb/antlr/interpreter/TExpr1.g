tree grammar TExpr1;

options {
	tokenVocab=Expr;

	ASTLabelType=CommonTree;
    superClass=MyTreeParser;
}

@header {
package tb.antlr.interpreter;
}

prog    : (expr)* ;

expr returns [Integer out]
	      : INT {$out = getInt($INT.text);}
	      | (name=ID) {$out = symbols.getSymbol($name.text);}
        | ^(PLUS  e1=expr e2=expr) {$out = $e1.out + $e2.out;}
        | ^(MINUS e1=expr e2=expr) {$out = $e1.out - $e2.out;}
        | ^(MUL   e1=expr e2=expr) {$out = $e1.out * $e2.out;}
        | ^(DIV   e1=expr e2=expr) {$out = $e1.out / $e2.out;}
        | ^(AND   e1=expr e2=expr) {$out = ($e1.out * $e2.out) == 0 ? 0 : 1;}
        | ^(MOD   e1=expr e2=expr) {$out = calculateModulo($e1.out, $e2.out);}
        | ^(EXP   e1=expr e2=expr) {$out = calculatePower($e1.out, $e2.out);}
        | ^(PRINT e1=expr)         {print($e1.out);}
        | ^(VAR name=ID)           {symbols.newSymbol($name.text);}
        | ^(PODST name=ID e1=expr) {symbols.setSymbol($name.text, $e1.out);}
//        | ^(PODST i1=ID   e2=expr)
        ;
