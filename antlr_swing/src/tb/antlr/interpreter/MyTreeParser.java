package tb.antlr.interpreter;

import java.util.Optional;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;

import tb.antlr.symbolTable.GlobalSymbols;

public class MyTreeParser extends TreeParser {
	
	protected GlobalSymbols symbols = new GlobalSymbols();

    public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

    protected void drukuj(String text) {
        System.out.println(text.replace('\r', ' ').replace('\n', ' '));
    }

	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
	
	protected void print(Object o) {
    	Optional<Object> opt = Optional.ofNullable(o);
    	if(opt.isPresent()) {
        	System.out.println(opt.get().toString());
    	}
    }
	
	protected int calculateModulo(int a, int b) {
    	return a % b;
    }
	
	protected int calculatePower(int number, int exponent) {
    	return (int) Math.pow(number, exponent);
    }
}
