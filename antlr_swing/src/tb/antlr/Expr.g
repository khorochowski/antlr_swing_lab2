grammar Expr;

options {
  output=AST;
  ASTLabelType=CommonTree;
}

@header {
package tb.antlr;
}

@lexer::header {
package tb.antlr;
}

prog
    : (stat )+ EOF!;

stat
    : expr NL -> expr
    
    | if_condition NL -> if_condition
    | VAR ID PODST expr NL -> ^(VAR ID) ^(PODST ID expr)
    | VAR ID NL -> ^(VAR ID)
    | ID PODST expr NL -> ^(PODST ID expr)
    | doWhileLoop NL -> doWhileLoop
    | whileLoop NL -> whileLoop
    | forLoop NL -> forLoop

    | NL ->
    ;
    
if_condition
    : IF^ if_expr THEN! stat (ELSE! stat)?
    ;

if_expr
    : expr
    ( EQUAL^ expr 
    | NOT_EQUAL^ expr
    )*
    ;

expr
    : basicExpr
      ( CMP^ basicExpr
      | NCMP^ basicExpr
      )*
    ;
    
basicExpr
    : multExpr
      ( PLUS^ multExpr
      | MINUS^ multExpr
      )*
    ;

multExpr
    : atom
      ( MUL^ atom
      | DIV^ atom
      )*
    ;

atom
    : INT
    | ID
    | LP! expr RP!
    ;

doWhileLoop
    :DO^ expr WHILE! expr
    ;

whileLoop
    :WHILE^ expr DO! expr
    ;
    
forLoop
    :FOR^ expr DO! expr
    ;
    
VAR :'var';

DO :'do';

WHILE :'while';

FOR :'for';

CMP :'==';

NCMP :'!=';

IF :'if';

ELSE :'else';

THEN :'then';



ID : ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*;

INT : '0'..'9'+;

NL : '\r'? '\n' ;

WS : (' ' | '\t')+ {$channel = HIDDEN;} ;


LP
  : '('
  ;

RP
  : ')'
  ;

PODST
  : '='
  ;

PLUS
  : '+'
  ;

MINUS
  : '-'
  ;

MUL
  : '*'
  ;

DIV
  : '/'
  ;